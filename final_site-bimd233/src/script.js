$(document).ready(function() {
  $("li").css("id", "lav");
  const states = ["idle", "gather", "process"];
  var state = states[0];
  var words = new Array();
  var ndx = 0;

  $("ul").on("mouseover", "li", function() {
    console.log("x:" + $(this).text());
    $(this).attr("id", "lav");
  });
  $("ul").on("mouseleave", "li", function() {
    $(this).attr("id");
  });

  //reset button click

  $("button").on("click", function(e) {
    $("li").remove();
  });

  var str = " ";

  //keypress
  $("input").on("keypress", function(e) {
    var code = e.which;
    var char = String.fromCharCode(code);

    str += char;
    if (code === 13) {
      var liList = "<li class = 'something'>" + str + "</li>";

      str = "";

      $("ul").append(liList);

      $("input").val("");
    }

    switch (state) {
      //idle
      case "idle":
        break;

      //gather
      case "gather":
        break;

      //process
      case "process":
        break;

      default:
        break;
    }
  });
});

//todo list

function artwork(art, title, date, type) {
  this.art = art;
  this.title = title;
  this.date = date;
  this.type = type;
}

var art1 = new artwork(
  '<img src = "https://cdn.discordapp.com/attachments/677255760916512802/690020871921729542/kiyoshi_kawai.png"width="220px" height="300px">',
  "Kiyoshi Kawai",
  "Dec 27 2019",
  "Rough Sketch"
);

var art2 = new artwork(
  '<img src = " https://cdn.discordapp.com/attachments/677255760916512802/690020818889080833/minh_ton_birthday_antei_uwu.png" width="280px" height="300px">',
  "Happy Birthday Minh Ton",
  "Nov 10 2019",
  "Rough Sketch"
);

var wx_data = [art1, art2];

var table = document.getElementById("table_data");
table.innerHTML =
  '<tr id="ident"> <th scope="col">Art</th> <th scope="col">Title</th> <th scope="col">Date</th> <th scope="col">Type</th>';

for (var i = 0; i < wx_data.length; i++) {
  table.innerHTML +=
    '<tr><th scope="row">' +
    wx_data[i].art +
    "</td><td>" +
    wx_data[i].title +
    "</th><td>" +
    wx_data[i].date +
    "</td><td>" +
    wx_data[i].type +
    "</td><td>";
}

//figuring out where to put an array

function updateWeather() {
  var myURL = "https://api.weather.gov/stations/KSFO/observations/latest";
  //var myURL = "https://api.wunderground.com/us/wa/bothell/zmw:98021.1.99999/latest";

  //if it isn't bothell that means it didn't work

  $("ul li").each(function() {
    $("li").remove();
  });

  $.ajax({
    url: myURL,
    dataType: "json",
    success: function(data) {
      var tempC = data["properties"]["temperature"].value.toFixed(1);
      var tempF = ((tempC * 9) / 5 + 32).toFixed(1);

      var windDirection = data["properties"]["windDirection"].value.toFixed(1);
      var windSpeed = (data["properties"]["windSpeed"].value * 1.94384).toFixed(
        1
      );
      var relativeHumidity = data["properties"][
        "relativeHumidity"
      ].value.toFixed(1);

      var myJSON = JSON.stringify(data);
      $("textarea").val(myJSON);

      var str =
        +"<li>Current temperature: " +
        tempC +
        "C " +
        tempF +
        "F" +
        "</li>" +
        "<li>Humidity: " +
        relativeHumidity +
        " %RH" +
        "</li>" +
        "</li>" +
        "<li>Current Wind: " +
        windDirection +
        " degrees at " +
        windSpeed +
        "kts" +
        "</li>";

      $("ul").append(str);

      $("ul li:last").attr("class", "list-group-item");
    }
  });
}

//weather shit

//    old carousel shit

//   <div id="carousel" class="carousel slide" data-ride="carousel">

//     <ol class="carousel-indicators">

//       <li data-target="#carousel" data-slide-to="0" class="active"> </li>
//       <li data-target="#carousel" data-slide-to="1"> </li>
//       <li data-target="#carousel" data-slide-to="2"> </li>

//     </ol>

//     <div class="carousel-inner">
//       <!-- Space -->
//       <div class="carousel-item active">
//         <img class="d-block w-100" id="carousel-image" src="https://cdn.discordapp.com/attachments/677255760916512802/690014885311938560/Renjyuu_watercolor_thing.png" alt="Slide 1">
//         <div class="carousel-caption">
//           <p>Calm</p>
//         </div>
//       </div>
//       <!-- Space -->
//       <div class="carousel-item">
//         <img class="d-block w-100" id="carousel-image" src="https://cdn.discordapp.com/attachments/677255760916512802/690014852285988935/momo_panic_AOAPP.png" alt="Slide 2">
//         <div class="carousel-caption">
//           <p>Panic! In the website HTML</p>
//         </div>
//       </div>
//       <!-- Space -->
//       <div class="carousel-item">
//         <img class="d-block w-100" id="carousel-image" src="https://cdn.discordapp.com/attachments/677255760916512802/690014686166515738/dramatic_sona_shit_boom.png" alt="Slide 3">
//         <div class="carousel-caption">
//           <p>Flare Bloom</p>
//         </div>

//       </div>
//     </div>

//     <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
//       <span class="carousel-control-prev-icon" aria-hidden="true"></span>
//       <span class="sr-only">Previous</span>
//     </a>
//     <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
//       <span class="carousel-control-next-icon" aria-hidden="true"></span>
//       <span class="sr-only">Next</span>
//     </a>

//   </div>

//Fade effects

//  $(document).ready(function() {
//   $("img").css("margin", "10px");
//   $("img").attr("id", "lav");

//  $("#p2 li").click(function() {
//     console.log("$(this):" + $(this));

//     $(this).fadeOut(500, function() {
//       console.log("fadeout complete!");
//     });
//   });

//   $("#p2 li").click(function() {
//     console.log("$(this):" + $(this));

//     $(this).fadeIn(2000, function() {
//       console.log("fadein complete!");
//     });
//   });

//Fadeout effects
